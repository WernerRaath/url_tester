import os, sys
import traceback
import logging
import argparse

import requests
from requests.exceptions import HTTPError
from yaml import load, dump

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

logging.basicConfig(level=logging.INFO, format='%(asctime)s: %(message)s', datefmt='%d-%b-%y %H:%M:%S')

def test_url(url, _type, data, response_type=None):
    try:
        if _type == "GET":
            response = requests.get(url)
        elif _type == "POST":
            response = requests.post(url, data=data)
        else:
            raise Exception("Unsupprted type of HTTP request")
        status = response.status_code
        message = ""
        
        _response_type = response.headers.get("Content-Type")
        if response_type is not None and _response_type.find(response_type) < 0:
            message = f"Incorrect Response Type: {_response_type}"
            status = 400
    except HTTPError as http_err:
        e = http_err.read().decode('utf-8')
        status = 500
        message = e
    except Exception as err:
        message = err
        status = 400
    return status, message

def main(path, config):
    path = path[path.rfind("/")+1:path.rfind(".")]
    logs = {
        "failed": 0,
        "total": 0,
        "s": {}
    }
    if config is not None:
        for service in config:
            request = config[service]
            request_type = request.get("type", "get").upper()
            logs["s"][service] = {
                "failed": 0,
                "total": 0
            }

            data_list = request.get("data", [])
            if not isinstance(data_list, list):
                data_list = [data_list]
            host_list = request.get("hosts", [])
            if not isinstance(host_list, list):
                host_list = [host_list]
            
            if len(data_list) == 0:
                data_list = [{}]
            if len(host_list) == 0:
                host_list = [""]
            
            response_type = request.get("response_type", None)

            for host in host_list:
                for data in data_list:
                    url = host + request["url"].format(**data).replace(" ", "%20")
                    status, message = test_url(url, request_type, data, response_type)
                    if message:
                        message = f": {message}"
                    logging.info(f"{path} {service} {status} = {url} {message}")
                    if status != 200:
                        logs["s"][service]["failed"] += 1
                    logs["s"][service]["total"] += 1
            logs["total"] += logs["s"][service]["total"]
            logs["failed"] += logs["s"][service]["failed"]
    
    for service in logs['s']:        
        logging.info(f"{path} {service} TOTAL: {logs['s'][service]['total']} | FAILED: {logs['s'][service]['failed']}")
    logging.info(f"{path} TOTAL: {logs['total']} | FAILED: {logs['failed']}")
    if logs["failed"] > 0:
        return 1
    else:
        return 0

parser = argparse.ArgumentParser(description='URL testing tool')
parser.add_argument('config_path', nargs='+', help='Path to the config file to be used')
args = parser.parse_args()

paths = args.config_path
exit_codes = []
for path in paths:    
    config = load(open(path), Loader=Loader)
    exit_codes.append(main(path, config))

sys.exit(max(exit_codes))