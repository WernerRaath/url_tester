FROM python:3.8-slim-buster
ADD ./src /app
ADD ./tests /tests
WORKDIR /app
RUN pip install pyyaml requests \
    && python test.py /tests/*